package br.com.zup.MentoraLilian.dominio;

public class ContaBancaria {

    private String numeroDaConta;
    private String nomePessoa;
    private String banco;
    private String agencia;
    private String pix;


    public ContaBancaria(String numeroDaConta, String nomePessoa, String banco, String agencia, String pix) {
        this.numeroDaConta = numeroDaConta;
        this.nomePessoa = nomePessoa;
        this.banco = banco;
        this.agencia = agencia;
        this.pix = pix;
    }


    public String getNumeroDaConta() {
        return numeroDaConta;
    }

    public void setNumeroDaConta(String numeroDaConta) {
        this.numeroDaConta = numeroDaConta;
    }

    public String getNomePessoa() {
        return nomePessoa;
    }

    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getPix() {
        return pix;
    }

    public void setPix(String pix) {
        this.pix = pix;
    }



    public String imprimir(){

        System.out.println("Numero da Conta: " + this.numeroDaConta);
        System.out.println("Nome: " + this.nomePessoa);
        System.out.println("Banco: " + this.banco);
        System.out.println("Agência: " + this.agencia);
        System.out.println("Chave pix: " + pix);

        return " ";

    }


    @Override
    public String toString() {
        return imprimir();
    }
}
