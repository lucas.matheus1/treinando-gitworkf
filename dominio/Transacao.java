package br.com.zup.MentoraLilian.dominio;

import java.math.BigDecimal;

public class Transacao {

    private ContaBancaria contaDebito;
    private ContaBancaria contaCredito;
    private BigDecimal valorTransacao;
    private ContaBancaria pix;


    public ContaBancaria getContaDebito() {
        return contaDebito;
    }

    public void setContaDebito(ContaBancaria contaDebito) {
        this.contaDebito = contaDebito;
    }

    public ContaBancaria getContaCredito() {
        return contaCredito;
    }

    public void setContaCredito(ContaBancaria contaCredito) {
        this.contaCredito = contaCredito;
    }

    public BigDecimal getValorTransacao() {
        return valorTransacao;
    }

    public void setValorTransacao(BigDecimal valorTransacao) {
        this.valorTransacao = valorTransacao;
    }

    public ContaBancaria getPix() {
        return pix;
    }

    public void setPix(ContaBancaria pix) {
        this.pix = pix;
    }

    public void imprimiTransacao(){

        System.out.println("Conta Debito: " + contaDebito);
        System.out.println("Conta Credito: " + contaCredito);
        System.out.println("Valor da Transação: " + valorTransacao);

    }







}
