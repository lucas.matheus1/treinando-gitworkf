package br.com.zup.MentoraLilian.test;

import br.com.zup.MentoraLilian.dominio.ContaBancaria;
import br.com.zup.MentoraLilian.dominio.Transacao;

import java.math.BigDecimal;
import java.util.HashMap;

public class TestContaBancaria {

    public static void main(String[] args) {

        HashMap<String, ContaBancaria> contas = new HashMap<>();

        var pix = "123";

        // Instanciando de uma forma diferente a minha classe conta Bancaria, onde eu uso a minha chave e corpo HashMap.
        contas.put(pix, new ContaBancaria("00001", "André", "Itau"
                , "013", pix));


        ContaBancaria contaDebito = new ContaBancaria("00001", "Lucas", "Itau"
                , "013", "1342");



        ContaBancaria contaCredito = new ContaBancaria("00002", "Lilian", "Nubank"
                , "01", "1341");


        contas.put(contaDebito.getPix(), contaDebito);

        if (contas.containsKey(contaCredito.getPix())) {
            System.out.println("A conta e a mesma.");
        }
        else{
            contas.put(contaCredito.getPix(), contaCredito);
        }


        //Print normal
//        System.out.println("Contas cadastradas: " + contas.get(contaDebito.getPix()) + " " + contas.get(contaCredito.getPix()));

        //print com o for passando na chave HashMap
        for (String pecorreChave:contas.keySet()) {
            System.out.println("--------------------");
            System.out.println("Conta cadastrada: " + contas.get(pecorreChave).imprimir());

        }

        Transacao transacao = new Transacao();

        //Adicionando os valores na transação.
        transacao.setValorTransacao(new BigDecimal("100"));
        transacao.setContaDebito(contaDebito);
        transacao.setContaCredito(contaCredito);

        System.out.println("------------------------");



    }
}
